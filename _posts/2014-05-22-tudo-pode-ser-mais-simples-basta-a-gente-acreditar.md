---
layout: post
title: Tudo pode ser mais simples, basta a gente acreditar
excerpt: A diferença da opinião entre crianças e adultos sobre o futuro do mundo é tão grande que eu acredito ser uma criança que cresceu demais, porque não vejo as cosias da mesma forma que os adultos veem.
date: 2014-05-22 22:56
published: false
tags: Simplify
---
Acabei de assistir um vídeo que uma amiga postou no Facebook e fiquei feliz, mas ao mesmo tempo horrorizado com o que eu vi.

<iframe class="article__video" src="//player.vimeo.com/video/78245147?color=1abc9c&portrait=0" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

Eu sempre fui uma pessoa extremamente otimista. Obviamente isso foi mudando aos poucos com o tempo, mas eu nunca deixei de ser quem eu sou, sempre tentando acreditar que as coisas seriam possíveis para mim e para aqueles mais próximos, principalmente.

Depois de assistir esse vídeo eu entendo porque sou tão apaixonado por crianças. Elas tem um brilho nos olhos que os adultos não tem. Crianças acreditam nas pessoas, tem esperança que tudo pode dar certo e o mais legal, elas tem uma imaginação sem limites! Esse brilho a gente nunca deveria perder. Acreditar em algo é o que faz com que as coisas possam ser diferentes.

Imagine você, adulto, que está lendo esse texto, se seus pais nunca tivessem acreditado em você. Que você nunca seria capaz de crescer, de se tornar um bom profissional, uma pessoa de bem. Provavelmente você teria um fim trágico, ou seria uma pessoa amarga e, muito provavelmente, não saberia ser uma pessoa feliz, mesmo com todas as dificuldades que tem hoje.

Não acho que todo mundo tenha que dormir com o Bozo e acordar sorrindo para as paredes. Aliás, é irritante pessoas excessivamente felizes, convenhamos, isso não é normal. Temos nossos dias em que acordamos do avesso e queremos socar o primeiro que vem com aquele &#8220;bom dia&#8221; e um sorriso estampado no rosto. Somos seres humanos, oras, normal ter um dia ruim. Mas nem por isso devemos deixar de acreditar que o mundo pode se tornar um lugar melhor.

Penso que quando crescemos e ficamos adultos desenvolvemos um senso crítico que nos ajuda a transformar momentos ruins em bons. As crianças não tem esse senso crítico, então a única arma delas é acreditar, dia após dia, que tudo vai melhorar. Nós, adultos, temos a capacidade de enxergar o que de ruim está acontecendo e o que devemos fazer para mudar. E por isso temos a obrigação de alimentar as esperanças das nossas crianças e trabalhar duro para que o mundo seja um lugar melhor pra elas.

Nos comentários do vídeo, fiquei muito triste quando uma pessoa disse algo que não vou copiar aqui, já que além de ser totalmente oposto do que eu penso, me deixou pesado só de ler, como se tivesse uma mochila nas minhas costas. Mas você pode <a href="https://vimeo.com/78245147#comment_10643390" title="Falta de esperança no mundo" target="_blank" rel="nofollow">ler esse comentário bem aqui</a>.

Enquanto escrevia esse post eu pensei bastante e cheguei à conclusão de que enquanto nós continuarmos como esses adultos do vídeo, o mundo nunca será um lugar melhor e, pior que isso, nossos filhos viveram em um mundo pior que o que temos hoje. É isso que você quer para seus filhos?

<strong class="highlight">Ainda dá tempo de mudar! ;)</strong>
