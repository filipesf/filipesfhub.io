---
layout: post
title: Como a paternidade me ensinou a rever minhas prioridades
excerpt: Tem vezes que a vida não nos dá escolhas e temos que fazer de tudo para que a cada tomada de decisão seja uma nova oportunidade de crescimento em busca de uma vida mais equilibrada para nós e nossa família.
date: 2014-10-09 23:10
published: false
tags: Work
---
Olha, se tem uma coisa que a paternidade faz com a gente é acelerar o processo de amadurecimento. Acho que a gente ganha um ano de experiência de vida pra cada mês passa depois que nossos filhos nascem. Se minhas contas tiverem certas, eu estou beirando os 38 anos. Haha!

Bom, brincadeiras a parte, vim aqui hoje pra registrar um momento da minha vida que achei que não fosse acontecer tão cedo: aceitar uma proposta de emprego.

> Nossa, Filipe! Grande coisa. O que tem demais nisso?

Muita coisa! Faz pouco mais de 4 anos que larguei essa vida de trabalhar para os outros em agência, para focar nos meus clientes. Nesse tempo eu cresci bastante como profissional, aprendi coisa pra caramba e claro, conheci pessoas sensacionais. Uma dessas pessoas é a senhora minha esposa, <a href="http://twitter.com/camilla_wolf" title="Camilla Wolf no Twitter" target="_blank" rel="nofollow">Camilla Wolf</a>. <i class="fa fa-heart text-color--alert"></i>

Nesse tempo todo eu sempre fiz da minha vida o que eu estava afim de fazer. Só pegava projeto que eu achava que valia a pena, mesmo quando eu estava meio apertado de grana. Antes de mergulhar de cabeça na vida de freelancer, passei um tempo quicando em várias agências. Em cerca de um ano passei por 5 agências diferentes. Não, eu não acho isso bonito, mas se tem uma coisa que me incomoda é não estar satisfeito com meu trabalho.

Sempre considerei que trabalhar com o que a gente gosta e como a gente gosta deveria ser a regra e não a excessão, pois só fazendo dessa forma vamos colocar todo nosso potencial em prática e desenvolver grandes projetos. Do contrário, seremos pessoas, profissionais medíocres e, sendo muito otimista, só faremos projetos medianos.

Claro que nem tudo é lindo e colorido como num conto de fadas. Problemas se tem trabalhando em agência, empresas privadas, órgãos públicos e até mesmo como freelancer da forma que a gente quer. Não existe trabalho perfeito. Mas precisamos buscar sempre aquele ambiente e modelo de trabalho que potencialize ao máximo o que nós sabemos fazer.

Se você leu até aqui deve estar se perguntando&#8230;

> Onde será que ele quer chegar?

Bom, acontece que quando se é pai, ou mãe, as coisas não funcionam bem assim. Fim do mês as contas tem que estar pagas, geladeira cheia e você tem que ter o fim de semana livre para programas familiares, rever os amigos, etc. E aquela pessoa que um dia precisava se preocupar só consigo mesma e a carreira de repente tem que se desdobrar em 10, 100, 1000 e conseguir fazer isso tudo.

E nessa de ter que se dividir, fica cada vez mais difícil encarar o trabalho da mesma forma. Em especial quando se é empreendedor.

Empreender é doloroso não só para quem o faz, mas isso atinge todos que estão por perto já que demanda MUITA dedicação, suor e lágrimas. Não que empreender seja ruim, de forma alguma, mas as recompensas demoram a vir e muitas vezes não temos esse tempo para esperar.

O que eu quero dizer é que quando se constitui uma família, às vezes precisamos abrir mão da nossa inconsequência e pensar mais nos que estão próximos de nós. Temos que nos preocupar em fazer com que sua família tenha um mínimo de estabilidade. Empreender não te dá isso no início. É muita luta até se conseguir resultados sólidos e de longo prazo.

E por isso, pra mim, não foi tão simples assim aceitar essa oportunidade. Foram várias noites reflexivas pensando como será minha vida daqui pra frente, como eu posso, lá na frente, conciliar tudo com um emprego, fazer novos projetos e tudo mais. Sim, porque eu não vou abandonar o <a href="http://chocoladesign.com" title="Choco la Design | Design é como chocolate, deixa tudo mais gostoso." target="_blank" rel="bookmark">Choco la Design</a> por conta desse emprego, pelo contrário, quem acompanha nosso trabalho por lá verá MUITAS mudanças em 2015. Mas não vou dar *spoilers*. :P

Mas estou bem confiante e empolgado com essa nova empreitada. Vou precisas estudar bastante pois vou trabalhar com uma tecnologia que nunca trabalhei antes e segundo meu chefe, eu preciso ser &#8220;o cara&#8221;.

<h2>A nova jornada começa!</h2>

Aos curiosos e interessados na parte técnica dessa nova jornada, nessa segunda-feira (13/10/2014) começo a trabalhar no <a href="http://portalsaude.saude.gov.br/" title="AngularJS — Superheroic JavaScript MVW Framework" target="_blank" rel="nofollow">Ministério da Saúde</a> e serei um dos responsáveis pela parte de front-end dos projetos internos do Ministério e, também, a meta é me tonar um profissional fera em <a href="https://angularjs.org/" title="AngularJS" target="_blank" rel="nofollow">AngularJS</a>. Vou precisas parar meus estudos com <a href="http://www.rubyonrails.com.br/" title="Ruby on Rails" target="_blank" rel="nofollow">Ruby on Rails</a> por um tempo, mas não vou abandonar.

Decidi aceitar principalmente porque acho que além de uma grande oportunidade, é a hora de eu dar uma acalmada e estabilizar um pouco minha vida e só depois voltarei a me aventurar *fulltime* nas minhas empreitadas. Vai ser algo totalmente novo pra mim, já que vou ter que mudar minha rotina de trabalho e aprender novas tecnologias, mas estou feliz e bem empolgado. :)

Espero que tenham curtido o texto de hoje, acabou sendo mais um desabafo sobre essa vida paternal nada simples, mas eu precisava dar uma passada aqui de novo, tirar as teias de aranha e voltar a escrever. Faz um bem danado pra mim. :)
