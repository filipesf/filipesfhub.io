---
layout: post
title: "Seja a mudança"
excerpt: As pessoas acreditam que podem mudar umas as outras, mas esquecem que antes de mudar uma sociedade precisamos mudar a nós mesmos.
date: 2015-02-09 17:13:02
published: false
tags: Society
---

Há cerca de 4 anos eu tenho lido **muito** sobre tudo que ajude a trazer autoconhecimento. Já li sobre astrologia, religiões, coaching, liderança, empreendedorísmo e uma infinidade de outros assuntos.

Toda essa busca por autoconhecimento me ajudou a desenvolver um hábito de que em tudo que leio, faço, escuto ou vejo, trago para minha realidade, tento tirar uma lição e procuro refleltir de como certas atitudes, tanto minhas quanto dos outros, impactam na minha vida. E a maior lição que tirei desses últimos anos de observação foi que:

> Palavras por palavras, ninguém muda ninguém.

O que mudam pessoas são atitudes, é por à prova aquilo que se diz. De que adianta você falar de família, quando na primeira oportunidade você trai seu(sua) companheiro(a)? É, ou quer ser, o chefe da sua equipe no trabalho, mas apenas cuida dos seus interesses ao invés de colaborar com seus companheiros?

De tudo que já vi/li por aí, o que mais me abriu os olhos foi o empreendedorísmo. Ter uma empresa é a maior prova de que se você não fizer algo, ninguém vai fazer. O mesmo vale para nós como pessoas.

A gente está sempre preocupado com os outros e esquecemos de nós, das nossas falhas e erros. Queremos mudar o próximo sem mudar a nós mesmos. E com isso a gente volta ali atrás onde falei que palavras não mudam ninguém, você precisa de atitudes.

Me senti motivado a escrever esse post depois de assistir o vídeo abaixo, do Bob Fernandes, entrevistado para o documentário "O mercado de notícias", de Jorge Furtado. O vídeo é um pouco extenso, mas se quiser, <a href="https://www.facebook.com/video.php?v=10204961234649368" title="A corrupção no Brasil" target="_blank" rel="nofollow">pode conferir o trecho a que me refiro</a>.

<iframe class="article__video" src="https://www.youtube.com/embed/QG4CzutWUQ4" frameborder="0" allowfullscreen></iframe>

Acredito que essa foi a definição mais sensata do que tem causado a corrupção no Brasil. E uma coisa é fato: um problema puxa o outro. No fim, essa bomba econônica em que estamos traz diversos problemas e o principal deles é social.

<h3 class="article__heading">O problema</h3>

Andei observando que o que retrata essa realidade muito bem é uma questão muito simples: a fila do ônibus.

Até então haviam duas filas no ônibus que pego todos os dias às 6h30 da manhã: a fila a de quem quer ir sentado e a de quem quer ir em pé. A fila de ir sentado pára assim que quem estiver na frente ver que não conseguirá um acento e é liberada para entrar antes da outra fila. Depois, a fila de ir em pé é liberada e é mais rápida, afinal todos tem horário para cuprir e acabam indo do jeito que dá. E esse sistema sempre funcionou muito bem até pouco tempo.

Recentemente a empresa responsável por essa linha resolveu que não teria mais fila, agora é que nem o metrô: fica todo mundo amontoado na porta e quem entrar entrou.

O grande problema é que quando haviam as filas, sempre teve um fiscal que se certificava de que as pessoas estavam respeitando a ordem de entrada das filas e se os passageiros preferenciais entravam antes de todos, além de manter a civilidade entre as pessoas. Depois que a empresa tomou essa decisão de acabar com as filas, o terminal de ônibus virou o caos!

As pessoas simplesmente não respeitam nada nem ninguém. É um empurra empurra danado. Outro dia <a href="" ref="http://twitter.com/camilla_wolf" target="_blank" title="Camilla Wolf" rel="nofollow">Camilla</a> estava no ônibus com o Benjamin e foi praticamente atropelada por um senhor que não fez questão de nem ao menos pedir desculpas e ainda achou ruim quando Camilla reclamou com ele.

O que isso mostra é que estamos todos cobrando nossos governantes quando nós mesmos não somos os exemplos que queremos ver no poder. Seguindo a linha do exemplo que o Bob Fernandes deu, se uma pessoa que tem atitudes como a dessas pessoas que empurram as outras sem cerimônias for para o poder, vai ser capaz de atos tão corruptos quanto os nossos atuais governantes.

<h3 class="article__heading">A mudança</h3>

O ponto que me deixa mais preocupado é que os brasileiros, de maneira geral, precisam de alguém dizendo o que se deve fazer. Raras são as pessoas que tomam atitudes para resolver algum problema, mas todas tem bala na aguluha na hora de reclamar de algo. Quer dizer, precisa-se de um fiscal para que as pessoas se respeitem. Ninguém tem autonomia, afinal, o que impede os passageiros do ônibus se organizarem entre si e manter a fila funcionando como sempre funcionou? Onde está a mentalidade das pessoas de que elas mesmas devem resolver seus problemas?

Não estou aqui para falar deste ou daquele político, até porque pra ser bem sincero, não tenho muita esperança no modelo político atual, mas enquanto praticarmos pequenos atos corruptos como furar fila e tantos outros por menores, nosso país vai continuar exatamente onde está.

<br>

> Seja a mudança que você quer ver no mundo. <br>–Mahatma Gandhi
