---
layout: post
title: Como é bom estar de volta!
excerpt: Os maiores inimigos do crescimento profissional são a falta de motivação e o comodismo. É quando a pessoa não sabe aonde quer chegar e se contenta em ficar aonde está.
date: 2014-05-14 23:27
published: false
tags: Work
---
Foi com esse pensamento que passei um tempo tentando outras coisas na minha vida profissional e tentei sair um pouco da minha zona de conforto. Apesar dessa minha vontade maluca de querer fazer coisas novas, elas nem sempre precisam ser algo totalmente diferente do habitual, o que tornam as experiências muito menos frustrantes.

Para quem já acompanha meu trabalho há algum tempo sabe que passei o último ano com meu foco na gestão de marcas pessoais. Bom, nesse período li alguns bons livros, assisti uma pancada de palestras, participei de vários workshops sobre branding e coaching, consegui alguns clientes e como todo autodidata, fui com a cara e a coragem.

Não foi a fase mais rentável da minha vida profissional, mas com certeza me trouxe um crescimento muito grande em um período de tempo muito curto. E foi esse aprendizado que fez eu me dar conta de que essa nova habilidade de saber lidar com marcas pessoais não era bem o trabalho que eu acreditava pra mim.

É extremamente gratificante ver pessoas se recolocando no mercado de trabalho, sabendo seus pontos fortes e fracos, além de saberem também comunicar melhor quem são enquanto marca. Mas o que me enche os olhos mesmo é poder criar o que eu quiser. É poder ter uma ideia e torna-la realidade. E a única atividade que pode me dar esses &#8220;poderes mágicos&#8221; é o design + desenvolvimento.

No início do ano eu passei cerca de duas semanas em uma bolha, praticamente, trabalhando dia e noite no redesign do <a href="http://chocoladesign.com/" title="Design é como chocolate, deixa tudo mais gostoso." target="_blank">Choco la Design</a> junto com o <a href="http://twitter.com/willianmatiola" title="Willian Matiola" target="_blank">Willian</a>. Esse tempo me exigiu demais profissionalmente porque eu já estava praticamente parado há um bom tempo e em se tratando de tecnologia, um dia parado pode significar uma mudança considerável no mercado. Então precisei estudar e trabalhar no layout ao mesmo tempo, o que torna tudo mais desgastante. No fim deu tudo certo, o redesign foi sucesso e os leitores tem tido um engajamento ainda maior.

Essa incubação no Choco no início do ano despertou em mim novamente aquela sede pelo nosso conhecido WWW. Comecei a trabalhar com design aos 15 anos, e depois de um tempo eu quis aprender a programar. Comecei com HTML na época da transição do Internet Explorer 5.5 para o 6.0. Depois descobri o CSS, o que completou minha paixão por desenvolver interfaces. E assim segui até uns 4 ou 5 anos atrás, que foi quando conheci o <a href="http://wordpress.org" title="Wordpress é amor!" target="_blank" class="fi-heart">WordPress</a>. Com essa descoberta eu descobri como é incrível poder criar algo que funciona e que não seja apenas bonitinho. Tive a oportunidade de criar diversos blogs e portais com o WordPress e eu não sei muita coisa de programação em <a href="http://pt.wikipedia.org/wiki/Php" title="O que é PHP?" target="_blank">PHP</a> para o tanto de coisas que consigo fazer com ele.

Mas como de PHP eu só sei o CMS WordPress, resolvi aprender uma linguagem de programação diferente. Já faz alguns meses que venho estudando <a href="https://www.ruby-lang.org/en/" title="O que é Ruby?" target="_blank">Ruby</a>. É uma linguagem de programação não tão conhecida como PHP, Python e outras, mas tanto aqui quanto no exterior, é uma linguagem muito promissora. Decidi aprender uma nova linguagem porque quero ser um profissional cada vez menos dependente de outros profissionais. Além disso, também não quero mais depender de ferramentas prontas para conseguir desenvolver sites dinâmicos. Quero ter a capacidade de desenvolver uma aplicação do zero e poder colocar pra funcionar qualquer projeto que eu tenha em mente, assim como os projetos dos meus clientes.

E para encerrar, fiz esse blog porque eu sempre gostei de expor alguns pensamentos e opiniões no Facebook, mas eles acabam se perdendo. Então resolvi criar um cantinho para descarregar o cérebro. Aqui não devo falar muitas coisas técnicas, pra isso vocês podem ver <a href="http://chocoladesign.com/author/filipesf" title="Veja todos meus posts no Choco la Design" target="_blank">meus posts no Choco</a>.

Espero que tenham curtido minha volta e até o próximo post! :D
