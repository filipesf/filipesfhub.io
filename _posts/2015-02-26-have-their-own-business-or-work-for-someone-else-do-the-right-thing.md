---
layout: post
cover: /img/own-business-or-others-company.jpg
title: "Have their own business or work for someone else: Do the right thing"
excerpt: Is not an easy choice when life want to teach us a lesson.
date: 2015-02-26 1:07:25
published: true
tags: Entrepreneurship
---

I just read this article at [Entrepreneur](http://www.entrepreneur.com) called [5 Reasons Why I Quit My Own Business to Work for Someone Else](http://www.entrepreneur.com/article/242951) and I've been thinking about the reasons why I've take the same decision 5 months ago. And the answer is: My life had to teach me a lesson and just now I can realize this.

Well, all those 5 reasons listed by [Brandon Turner](https://www.linkedin.com/in/brandonrturner) are on my list too, but I have three more reasons why I left my full time job as a freelancer and back to work in a company which is not mine.

<h3 class="article__heading">1. My son was born</h3>

When you work as a freelancer, you have a bunch of inconsistencies every day. But, unfortunately, the irregular date to receive your payments is the worst.

So until my son completed 1yo, I kept my work as a freelancer for an unique reason: my wife needed me more then ever at the first months. And this decision wasn't easy, was very tough.

After October 19th, 2013, I would never be alone anymore. From that time forward, I had another life to take care and I can't wake up every day thinking if my clients would pay me or not. I needed certainties and no doubts.

<h3 class="article__heading">2. Routine is something necessary</h3>

Having a family requires some routines or your personal life turns a mess.

Working as a freelancer, routine is almost an impossible thing. That's because you needs to do home stuff, keep the eyes in your kids, do the work and so on.

When you have a full time job, you have a time to wake up, time to work, time to go back to home, time for your family and you can keep everything in his own place.

<h3 class="article__heading">3. Reducing conflicts</h3>

Any kind of relationship brings conflicts, either with your boss, with your wife, with your friends, no matter who, conflicts happen. But have many ways to avoid unnecessary conflicts, one of those is with a full time job.

When you're out of home, you have some time for you, for your job. This moment, when we can stay alone, is excellent to keep our ideas organized, see other people and leave our problems from behind.

So, when you come home to find your family, you are refreshed, with a bunch of stories about your day and your wife (or husband) and kids as well.

<h3 class="article__heading">Conclusion</h3>

After think about what is the best, if keep your own business, or, if work for a company, I realized which depends of what you passing through in your life.

We always have pros and cons on both decisions, but it's up to you to find out what the ideal for what you are going through. Only you know the answer.
