---
layout: post
title: Prospects Coaching
excerpt: Prospects is a brazilian company that work with coaching for high performance. They help people reach their full potential in any area of their lives and bring actual results.
date: 2016-03-07 10:00
tags: Projects
---

## Logo

**Typography**

Open Sans is a versatile font that can be used in different contexts combined with your variety of weights and still do an effective communication.

**Icon**

The icon is the origin of their slogam "We're talking about results". This icon brings the idea of a balloon, like on comic books, with a chart inside. Which means that they're talking about something that brings results. This same balloon comes with the "P" format which references the name of the company.

**Colour**

Purple combines the calm stability of blue and the fierce energy of red. The colour purple is often associated with nobility, power, and ambition. Purple also represents meanings of wealth,creativity, wisdom, dignity, grandeur, pride and independence.

[![Prospects Coaching Logo]({{ baseurl }}/img/prospects-logo.png)]({{ baseurl }}/img/prospects-logo.png)

## Website

The main goal of this website was to present the company through the [concept of the Golden Circle](https://www.youtube.com/watch?v=u4ZoJKF_VuA) where we comunicate *Why > How > What*. **Why** the company excists; **How** they deliver their products; **What** is the results.

We used big and clear texts answering all those questions driven the user to understand **why** they must choose Prospects and **how** the company will deliver **results** to their clients.

[![Prospects Coaching Website]({{ baseurl }}/img/prospects-website.png)]({{ baseurl }}/img/prospects-website.png)
